//
//  ProductosCell.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/18/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class ProductosCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTittle: UILabel!
}
