//
//  HomeMenuCell.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/29/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class HomeMenuCell: UICollectionViewCell {
    @IBOutlet weak var noticeTitle: UILabel!
    @IBOutlet weak var noticeSubtitle: UILabel!
    
}
