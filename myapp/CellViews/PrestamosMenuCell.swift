//
//  PrestamosMenuCell.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/4/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class PrestamosMenuCell: UICollectionViewCell {
    
    @IBOutlet weak var prestamosInfo: UILabel!
    @IBOutlet weak var saldoInfo: UILabel!
    @IBOutlet weak var descuentosInfo: UILabel!
    @IBOutlet weak var estatusInfo: UILabel!
    @IBOutlet weak var estatusInfoEstatico: LeftBorderedLabel!
    @IBOutlet weak var descuentosRestanteslbl: LeftBorderedLabel!

}
