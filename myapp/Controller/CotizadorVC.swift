//
//  CotizadorVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/17/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CotizadorVC: UIViewController {
    
    let productosTableView: UITableView = {
        let tv = UITableView()
        return tv
    }()
    
    let identifier = "ProductosCellid"
    
    var array_productos = [ProductosModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Productos"
        setup()
        showHUD(progressLabel: "Cargando")
        downloadProductos()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    private func setup() {
        productosTableView.register(UINib(nibName: "CellProductos", bundle: nil), forCellReuseIdentifier: identifier)
        productosTableView.separatorStyle = .none
        productosTableView.backgroundColor = UIColor.white
        productosTableView.allowsMultipleSelection = false
        productosTableView.allowsSelection = true
        productosTableView.alwaysBounceVertical = false
        view.addSubview(productosTableView)
        productosTableView.fillSuperview()
        productosTableView.delegate = self
        productosTableView.dataSource = self
    
    }
    
    private func downloadProductos() {
        
        let headers: [String:String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "username":"ALKE0002",
            "token": Config.token
        ]
        
        let urlString  = "http://infoquest.dyndns.org:8091/ALGUGAPI/Productos/Cotizador/\(UserDefaults.standard.getUserName())"
        print(urlString)
        Alamofire.request(urlString, method: .post,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success:
                do {
                    let jsonData = try JSON(data: response.data!)
                    let rCode = jsonData["responseCode"].intValue
                    let rMessage = jsonData["responseMessage"].stringValue
                    let rData = jsonData["data"].arrayValue
                    
                    print("rCodeProductos: \(rCode) | rMessage: \(rMessage) rDatas: \(rData)")
                    
                    for elements in rData {
                        let id_prod = elements["id_prod"].stringValue
                        let nombre = elements["nombre"].stringValue
                        let descripcion = elements["descripcion"].stringValue
                        let tasa_ord_max = elements["tasa_ord_max"].stringValue
                        let tipo_tasa = elements["tipo_tasa"].stringValue
                        
                        self.array_productos.append(ProductosModel(id_prod: id_prod, nombre_producto: nombre, descripcion_producto: descripcion, tasa_ord_max: tasa_ord_max, tipo_tasa: tipo_tasa))
                    }
                    
                    self.productosTableView.reloadData()
                    self.dismissHUD(isAnimated: true)
                } catch {
                    print(error)
                }
                
                break
            case .failure(let error):
                self.showStandardDialog(title: "Error", mess: "Se ha detectado un error")
                print("Error wea: \(error)")
                
            }
        }
    }
}

extension CotizadorVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cotizacionVC = storyboard?.instantiateViewController(withIdentifier: "CotizacionVC") as! CotizacionVC
        
        cotizacionVC.id_producto = array_productos[indexPath.row].id_prod
        cotizacionVC.nombre_prestamo_txt = array_productos[indexPath.row].nombre_producto
        cotizacionVC.tasa_ordinaria_txt = array_productos[indexPath.row].tasa_ord_max
        
        navigationController?.pushViewController(cotizacionVC, animated: true)
       
    }
}

extension CotizadorVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_productos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! ProductosCell
        cell.selectionStyle = .none
        cell.title.text = "\(array_productos[indexPath.row].nombre_producto)"
        cell.subTittle.text = "\(array_productos[indexPath.row].descripcion_producto)"
        return cell
    }
    
    
}
