//
//  DetailHomeVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/9/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class DetailHomeVC: UIViewController {

    var titleLabel = String()
    
    let label: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "Avenir Next", size: 20)
        lbl.textColor = UIColor.gray
        lbl.textAlignment = .center
        lbl.layer.masksToBounds = false
        lbl.layer.shadowRadius = 2.0
        lbl.layer.shadowOpacity = 0.2
        lbl.layer.shadowOffset = CGSize(width: 1, height: 2)
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(titleLabel)
        view.addSubview(label)
        label.text = "\(titleLabel)"
        
        label.centerInSuperview(size: CGSize(width: 200, height: 60))
    
    }
    


}
