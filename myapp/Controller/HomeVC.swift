//
//  SecondViewController.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/5/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import Kingfisher
import SideMenu

class HomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //private var boatAnimation: LOTAnimationView?
    //var positionInterpolator: LOTPointInterpolatorCallback?

    var collectionViewModules: UICollectionView!
    
    var array_Modules = [ModulesModel]()
    
    var titleLabel = String()
    
    let identifier = "cellModules"
    let identifier2 = "cellModules2"
    
    let btnCloseSession: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "log_out"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentEdgeInsets = .init(top: 0, left: 40, bottom: 0, right: 8)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        return button
    }()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //title = "Algoritmo"

        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController

        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        
        
        //SideMenuManager.default.menuPushStyle = .subMenu
        //SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuEnableSwipeGestures = false
        SideMenuManager.default.menuPresentMode = .viewSlideInOut
        SideMenuManager.default.menuWidth = 300.0
        SideMenuManager.default.menuShadowOpacity = 0.0
        SideMenuManager.default.menuFadeStatusBar = true
        
        //SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //setupViewsNavegationC()
        setupCollectionView()
        
    }
    
    private func setupViewsNavegationC() {
        let buttonNavBarItem = UIBarButtonItem.init(customView: btnCloseSession)
        btnCloseSession.addTarget(self, action: #selector(goCloseSession), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [buttonNavBarItem]
    }
    
    @objc func goCloseSession() {
        
        UserDefaults.standard.removeUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationLogin = storyboard.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = destinationLogin
        
        self.dismiss(animated: true, completion: nil);
    }

    private func setupCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 344, height: 112  )
        
        collectionViewModules = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        
        collectionViewModules.isUserInteractionEnabled = true
        collectionViewModules.dataSource = self
        collectionViewModules.delegate = self
        //collectionViewModules.register(UINib(nibName: "CellHome", bundle: nil), forCellWithReuseIdentifier: identifier2)
        collectionViewModules!.register(ModulesCell.self, forCellWithReuseIdentifier: identifier)
        collectionViewModules!.backgroundColor = UIColor.colorWithHexString(hexStr: "#EBECED")
        self.view.addSubview(collectionViewModules!)
        
        collectionViewModules.fillSuperview()
        
    }

    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_Modules.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier2, for: indexPath) as! HomeMenuCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as!
            ModulesCell

        let url = URL(string: array_Modules[indexPath.row].moduleImage)
        cell.imagePublicity.kf.setImage(with: url!)
        //cell.noticeTitle.text = "Aviso \(indexPath.row)"
        //cell.noticeSubtitle.text = "\(array_Modules[indexPath.row].moduleName)"
        
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        print("Hola mundo")
        return cell
    }
    
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //segueToDetailHome
        
        titleLabel = array_Modules[indexPath.row].moduleName
        //self.performSegue(withIdentifier: "segueToDetailHome", sender: self)
        print("\(titleLabel)")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetailHome" {
            
            if let detailHVC = segue.destination as? DetailHomeVC {
                detailHVC.titleLabel = titleLabel
            }
        }
    }

}

extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 16, bottom: 5, right: 16)
    }
}

class ModulesCell: UICollectionViewCell {
    
    let cellView: UIView = {
        let view = UIView() //203,188,219 //188,178,203
        //view.backgroundColor = UIColor(r: 188.0, g: 178.0, b: 203.0)
        view.backgroundColor = UIColor.white
        //view.setCellShadow()
        return view
    }()
    
    let nameModuleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont(name: "Avenir Next Bold", size: 16)
        label.text = "$40/hour"
        label.textColor = .white
        label.textAlignment = .center
        //label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let imagePublicity: UIImageView = {
        let ig = UIImageView()
        //ig.contentMode = .scaleAs
        return ig
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        
        backgroundColor = UIColor(r: 245, g: 245, b: 245)
        
        self.addSubview(cellView)
        
        cellView.addSubview(imagePublicity)
        
        cellView.fillSuperview()
        imagePublicity.fillSuperview()
        
        //cellView.addSubview(nameModuleLabel)

        //cellView.fillSuperview(padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        //nameModuleLabel.centerInSuperview(size: CGSize(width: 90, height: 60))

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

public extension UIColor {
    public convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}

