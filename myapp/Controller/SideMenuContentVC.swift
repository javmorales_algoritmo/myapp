//
//  SideMenuContentVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/21/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import Cards
import SideMenu
import Hero

class SideMenuContentVC: UIViewController {
    
    var tvMenuSide: UITableView!
    
    let MenuCellId = "cellMenu"
    
    let kHeaderSectionTag: Int = 6900;
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    
    struct cellData {
        var opened = Bool()
        var title = String()
        var sectionData = [String]()
        var nameImageIcon = String()
    }
    
    let card: CardHighlight = {
        let c = CardHighlight()
        //c.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
        c.backgroundColor = UIColor.white
        c.icon = UIImage(named: "algoritmo")
        c.title = "Bienvenido"
        c.itemTitle = "\(UserDefaults.standard.getCompleteName())"
        c.textColor = UIColor.white
        //c.hasParallax = true
        return c
    }()
    
    let viewTopUser: UIView = {
        let view = UIView()
        view.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor.lightGray, thickness: 1.0)
        return view
    }()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupTableViewMenu()
        
    }
    
    private func setupTableViewMenu() {
        tvMenuSide = UITableView()
        tvMenuSide.register(MenuCell.self, forCellReuseIdentifier: MenuCellId)
        tvMenuSide.separatorStyle = .none
        tvMenuSide.backgroundColor = UIColor.white
        tvMenuSide.allowsMultipleSelection = true
        tvMenuSide.allowsSelection = true
        tvMenuSide.alwaysBounceVertical = false
        
        tvMenuSide.tableFooterView = UIView()
        
        tvMenuSide.tableFooterView?.layer.addBorder(edge: UIRectEdge.top, color: UIColor.lightGray, thickness: 1.0)
        sectionNames = [ "Inicio","Mis Prestaciones","Mis Ajustes","Cerrar sesión"];
        sectionItems = [ [],
                         ["Mis Préstamos","Cotizar préstamo"],
                         ["Ajuste 1","Ajuste 2","Ajuste 3"],
                         []
        ];
        
        view.addSubview(card)
        view.addSubview(tvMenuSide)
        
        if #available(iOS 11.0, *) {
            card.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 120))
        } else {
           card.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 120))
        }
    
        tvMenuSide.anchor(top: card.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        tvMenuSide.dataSource = self
        tvMenuSide.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let index = tvMenuSide.indexPathForSelectedRow {
            self.tvMenuSide.deselectRow(at: index, animated: true)
        }
    }

    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        let tag = kHeaderSectionTag + section
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                
                //let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                let cImageView = eImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    
        switch tag {
        case 6900:
            print("Inicio")
        case 6904:
            UserDefaults.standard.removeUser()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destinationLogin = storyboard.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = destinationLogin
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            self.dismiss(animated: true, completion: nil);
            
        default:
            break
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tvMenuSide.beginUpdates()
            self.tvMenuSide.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tvMenuSide.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tvMenuSide.beginUpdates()
            self.tvMenuSide.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tvMenuSide.endUpdates()
        }
    }
}

extension SideMenuContentVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = "Retrieving data.\nPlease wait."
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = .center;
            messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
            messageLabel.sizeToFit()
            self.tvMenuSide.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.white
        header.textLabel?.textColor = UIColor.gray
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(SideMenuContentVC.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    //Este metodo no se borra, que hueva realizar todo de nuevo :V
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: self.MenuCellId, for: indexPath) as! MenuCell
        //cell.selectionStyle = .none
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.titleLabel.text = section[indexPath.row] as? String
        cell.tag = kHeaderSectionTag + indexPath.row
        cell.backgroundColor = UIColor.white
        return cell
        
    }

}

extension SideMenuContentVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("Aca andamos weon")
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sections = sectionItems[indexPath.section]
        print(sections)
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: sections)
        
        if let decodedArray = NSKeyedUnarchiver.unarchiveObject(with: encodedData) as? [Any] {
            
            let aux:String = "\(decodedArray[indexPath.row])"
            
            switch aux{
            case "Mis Préstamos":
                let vc = storyboard?.instantiateViewController(withIdentifier: "PrestamosVC")
                navigationController?.pushViewController(vc!, animated: true)
                
            case "Cotizar préstamo":
                let vc = storyboard?.instantiateViewController(withIdentifier: "CotizadorVC")
                navigationController?.pushViewController(vc!, animated: true)
            default:
                break
            }
        }
    }
    
 }

extension UIView {
    func setAnchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?,
                   bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?,
                   paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat,
                   paddingRight: CGFloat, width: CGFloat = 0, height: CGFloat = 0) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.topAnchor
        }
        return topAnchor
    }
    
    var safeLeftAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.leftAnchor
        }
        return leftAnchor
    }
    
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.bottomAnchor
        }
        return bottomAnchor
    }
    
    var safeRightAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.rightAnchor
        }
        return rightAnchor
    }
}

class MenuCell: UITableViewCell {
    
    let cellView: UIView = {
        let view = UIView() //203,188,219 //188,178,203
        //view.backgroundColor = UIColor(r: 188.0, g: 178.0, b: 203.0)
        view.backgroundColor = UIColor.colorWithHexString(hexStr: "#F1F1F2")
        view.setCellShadow()
        return view
    }()
    
    
    let pictureImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.font = UIFont(name: "Avenir Next", size: 15)
        //label.textColor = UIColor.gray
        label.textColor = UIColor.black
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    func setup() {
        backgroundColor = UIColor(r: 245, g: 245, b: 245)
        addSubview(cellView)
        cellView.addSubview(pictureImageView)
        cellView.addSubview(titleLabel)
        
        cellView.setAnchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 4, paddingLeft: 8, paddingBottom: 4, paddingRight: 8)
        
        pictureImageView.setAnchor(top: nil, left: cellView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 45, height: 40)
        pictureImageView.centerYAnchor.constraint(equalTo: cellView.centerYAnchor).isActive = true
        
        titleLabel.setAnchor(top: nil, left: pictureImageView.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, height: 40)
        titleLabel.centerYAnchor.constraint(equalTo: pictureImageView.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
