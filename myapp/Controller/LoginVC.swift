//
//  ViewController.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 10/29/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TKSubmitTransition
import AZDialogView
import SkyFloatingLabelTextField
import Hero

class LoginVC: UIViewController, UIViewControllerTransitioningDelegate {
    
    var jsonArray: NSArray?
    
    let tvUserName: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        txt.placeholder = "Usuario"
        txt.title = "Usuario"
        txt.font = UIFont(name: "Avenir Next", size: 16)
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.selectedTitleColor = UIColor.black
        txt.autocapitalizationType = .allCharacters
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    
    let tvUserPassword: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        txt.placeholder = "Contraseña"
        txt.title = "Contraseña"
        txt.font = UIFont(name: "Avenir Next", size: 16)
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.isSecureTextEntry = true
        txt.selectedTitleColor = UIColor.black
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    
    let btn_datas: TKTransitionSubmitButton = {
        let button = TKTransitionSubmitButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 64, height: 60))
        button.setTitle("Entrar", for: UIControl.State())
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 20)
        button.layer.cornerRadius = button.frame.height / 2
        button.addTarget(self, action: #selector(sendDatas(_:)), for: .touchUpInside)
        button.backgroundColor = UIColor(r: 188.0, g: 178.0, b: 203.0)
        return button
    }()
    
    let ivlogo: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "algoritmo")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
       return iv
    }()
    
    let namelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "ALGORITMO"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Avenir Next Demi Bold", size: 30)
        return lbl
    }()
    
    let btnChangeCustomerUser: UIButton = {
        let btn = UIButton()
        btn.setTitle("Cambiar usuario", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 14)
        btn.setTitleColor(UIColor.gray, for: .normal)
        btn.addTarget(self, action: #selector(goUIVC), for: .touchUpInside)
        return btn
    }()
    
    var isKeyboardAppear = false
    
    var array_Modules = [ModulesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setupTextView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //self.view.backgroundColor = UIColor.colorWithHexString(hexStr: "#EBECED")
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tvUserName.text = ""
        tvUserPassword.text = ""
    }
    
    
    @objc func goUIVC() {
        let userVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "vcUserInfo") as! UserInformationVC
        userVC.hero.isEnabled = true
        
        userVC.hero.modalAnimationType = .zoomSlide(direction: HeroDefaultAnimationType.Direction.left)
        self.hero.replaceViewController(with: userVC)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if !isKeyboardAppear {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
            isKeyboardAppear = true
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if isKeyboardAppear {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y != 0{
                    self.view.frame.origin.y += keyboardSize.height
                }
            }
            isKeyboardAppear = false
        }
    }
    
    private func setupTextView() {
        
        view.addSubview(tvUserName)
        view.addSubview(tvUserPassword)
        view.addSubview(btn_datas)
        view.addSubview(ivlogo)
        view.addSubview(namelbl)
        //view.addSubview(btnChangeCustomerUser)
        
        if #available(iOS 11.0, *) {
            namelbl.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24))
        } else {
            namelbl.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24))
        }
        
        ivlogo.anchor(top: namelbl.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 8, left: 40, bottom: 0, right: 40), size: .init(width: 270, height: 270))
        
        tvUserName.anchor(top: ivlogo.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 16, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 50))
        
        tvUserPassword.anchor(top: tvUserName.bottomAnchor, leading: tvUserName.leadingAnchor, bottom: nil, trailing: tvUserName.trailingAnchor, padding: .init(top: 32, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 50))
        
        btn_datas.anchor(top: tvUserPassword.bottomAnchor, leading: tvUserPassword.leadingAnchor, bottom: nil, trailing: tvUserPassword.trailingAnchor, padding: .init(top: 65, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 60))
    }
    
    
    private func validateUser() -> Bool {
        
        if (tvUserName.text?.isEmpty)! {
            
            tvUserName.errorColor = UIColor.red
            tvUserName.errorMessage = "Error"
            tvUserName.lineErrorColor = UIColor.red
            tvUserName.textErrorColor = UIColor.red
            return true
        }
        
        return false
    }
    
    private func validatePassword() -> Bool {

        if (tvUserPassword.text?.isEmpty)! {
            tvUserPassword.errorColor = UIColor.red
            tvUserPassword.errorMessage = "Error"
            tvUserPassword.lineErrorColor = UIColor.red
            tvUserPassword.textErrorColor = UIColor.red
            
            return true
        }else{
            
        }
        
        return false
    }
    
    @objc func sendDatas(_ sender: Any) {
        
        tvUserPassword.resignFirstResponder()
        tvUserName.resignFirstResponder()
        
        let params: [String : String] = [
            "UserName": "ALKE0002", //ALG0001
            "UserSecret": "ALPL-KEKE-0001" //ALPL-ALGM-0001 //Mobil2018
        ]
        
        if (validateUser() || validatePassword()) {
            print("Algo malo pasa")
            
        } else {
            btn_datas.startLoadingAnimation()
            let urlString = "http://infoquest.dyndns.org:8091/KEKUGAPI/Token"
            Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: nil ).responseJSON { response in
                
                switch response.result {
                case .success:
                    
                    do {
                        let json = try JSON(data: response.data!)
                        
                        let token = json["token"].stringValue
                        let rCode = json["responseCode"].intValue
                        let rMessage = json["responseMessage"].stringValue
                        
                        var dialog = AZDialogViewController()
                        switch rCode {
                        case 200:
                            Config.token = token
                            self.checkUserWithToken()
                        case 400:
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Error de API")
                        case 401:
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Error de bases de datos")
                        case 404:
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Algo falla")
                        case 403:
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Credenciales no autorizadas")
                        default: break
                            
                        }
                        
                    } catch {
                        print(error)
                    }
                    
                    break
                case .failure(let error):
                    
                    print("Error de la wea")
                    print(error)
                    
                }
            }
        }
        
        
    }
    
    public func stopLoadingAnimation() {
        
        btn_datas.setOriginalState()
        
    }
    
    func didFinishYourLoading() {
        btn_datas.startFinishAnimation(1) {
            self.performSegue(withIdentifier: "segueToHome", sender: self)
            //self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func checkUserWithToken() {
        
        let params: [String:String] = [
            "UserName": tvUserName.text!,
            "UserSecret": tvUserPassword.text!
        ]
        
        let headers: [String: String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "UserName":"ALKE0002",
            "Token": Config.token
        ]
        
        let urlString  = "http://infoquest.dyndns.org:8091/KEKUGAPI/LogIn"
        
        Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
        
            switch response.result {
            case .success:
                do {
                    let jsonData = try JSON(data: response.data!)
                    
                    let rCode = jsonData["responseCode"].intValue
                    let rMessage = jsonData["responseMessage"].stringValue
                    
                    let rData = jsonData["data"].arrayValue
                    print(jsonData)
                    for elements in rData {
                        
                        let UID = elements["UID"].intValue
                        let UName = elements["UName"].stringValue
                        let UEmail = elements["UEmail"].stringValue
                        
                        UserDefaults.standard.setUserID(value: UID)
                        UserDefaults.standard.setUserName(value: self.tvUserName.text!)
                        UserDefaults.standard.setLoggedIn(value: true)
                        UserDefaults.standard.setCompleteName(value: UName)
                        
                        print("\(UID) : \(UName) : \(UEmail)")
                        
                        let rElements = elements["UModules"].arrayValue
                    
                        for ele in rElements {
                            let id = ele["ModuleId"].intValue
                            let mi = ele["ModuleImage"].stringValue
                            let mn = ele["ModuleName"].stringValue
                            self.array_Modules.append(ModulesModel(moduleId: id, moduleName: mn, moduleImage: mi))
                        }
                        
                    }
                    
                    var dialog = AZDialogViewController()
                    
                    
                    switch rCode {
                    case 200:
                        
                        self.didFinishYourLoading()
                        print("Todo correcto")
                    case 400:
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Error de API")
                    case 401:
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Error de bases de datos")
                    case 404:
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Algo falla")
                    case 403:
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Credenciales no autorizadas - User")
                    default: break
                        
                    }
                    
                } catch {
                    print(error)
                }
                
                break
            case .failure(let error):
                print("Error wea: \(error)")
            
            }
        }
            
        
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToHome" {
            if let nVC = segue.destination as? UINavigationController {
                if let hVC = nVC.viewControllers.first as? HomeVC {
                    hVC.array_Modules = array_Modules
                }
            }
        }
    }
    
}

extension UITextField {
    @IBInspectable var doneAccessory: Bool {
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Listo", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}



