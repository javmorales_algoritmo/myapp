//
//  UserInformationVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/12/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Hero
import Alamofire
import AZDialogView
import SwiftyJSON

class UserInformationVC: UIViewController {
    
    let ivlogo: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "algoritmo")
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let welcomelbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Bienvenido de nuevo"
        lbl.textAlignment = .left
        lbl.font = UIFont(name: "Avenir Next Demi Bold", size: 25)
        return lbl
    }()
    
    let nameClientlbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "J*******"
        lbl.textAlignment = .left
        lbl.font = UIFont(name: "Avenir Next", size: 20)
        return lbl
    }()
    
    let tvUserPassword: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        txt.placeholder = "Contraseña"
        txt.title = "Contraseña"
        txt.font = UIFont(name: "Avenir Next", size: 16)
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.isSecureTextEntry = true
        txt.selectedTitleColor = UIColor.black
        //txt.addDoneButtonOnKeyboard()
        
        return txt
    }()
    
    let btnChangeCustomerUser: UIButton = {
        let btn = UIButton()
        btn.setTitle("Cambiar usuario", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 14)
        btn.setTitleColor(UIColor.gray, for: .normal)
        btn.addTarget(self, action: #selector(goLoginVC), for: .touchUpInside)
        return btn
    }()
    
    var array_Modules = [ModulesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAsterisk(value: UserDefaults.standard.getUserName())
        setupviews()
        hideKeyboardWhenTappedAround()
        addDoneButtonOnKeyboardAndSendDatas()
    
    }
    
    //segueHomeFromUser
    private func setAsterisk(value: String) {
        var aux_value = value
        let firstChar = value.first
        
        aux_value.remove(at: value.startIndex)
        
        var asterisk:String = ""
        
        for _ in aux_value.enumerated() {
            asterisk += "*"
        }
        
        nameClientlbl.text = "\(firstChar!)\(asterisk)"

    }
    
    private func setupviews() {
        [ivlogo, welcomelbl, nameClientlbl, tvUserPassword, btnChangeCustomerUser].forEach {view.addSubview($0) }
        
        if #available(iOS 11.0, *) {
            ivlogo.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24))
        } else {
            ivlogo.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24))
        }
        welcomelbl.anchor(top: ivlogo.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        nameClientlbl.anchor(top: welcomelbl.bottomAnchor, leading: welcomelbl.leadingAnchor, bottom: nil, trailing: welcomelbl.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        tvUserPassword.anchor(top: nameClientlbl.bottomAnchor, leading: nameClientlbl.leadingAnchor, bottom: nil, trailing: nameClientlbl.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 50))
        btnChangeCustomerUser.anchor(top: tvUserPassword.bottomAnchor, leading: tvUserPassword.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 25))
    }

    private func addDoneButtonOnKeyboardAndSendDatas() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Enviar", style: .done, target: self, action: #selector(doneButtonAction))
        let cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancelar", style: .done, target: self, action: #selector(cancelButtonAction))
        
        
        let items = [cancel, flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        tvUserPassword.inputAccessoryView = doneToolbar
    }
    
    @objc func cancelButtonAction() {
        tvUserPassword.resignFirstResponder()
        tvUserPassword.text = ""
    }
    
    private func validatePassword() -> Bool {
        
        if (tvUserPassword.text?.isEmpty)! {
            tvUserPassword.errorColor = UIColor.red
            tvUserPassword.errorMessage = "Error"
            tvUserPassword.lineErrorColor = UIColor.red
            tvUserPassword.textErrorColor = UIColor.red
            
            return true
        }
        return false
    }
    
    @objc func doneButtonAction() {
        let params: [String : String] = [
            "UserName": "ALKE0002", //ALG0001
            "UserSecret": "ALPL-KEKE-0001" //ALPL-ALGM-0001
        ]
        
        if (validatePassword()) {
            print("Algo malo pasa")
            
        } else {
            showHUD(progressLabel: "Cargando")
            let urlString = "http://infoquest.dyndns.org:8091/KEKUGAPI/Token"
            Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: nil ).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    
                    do {
                        let json = try JSON(data: response.data!)
                        
                        let token = json["token"].stringValue
                        let rCode = json["responseCode"].intValue
                        let rMessage = json["responseMessage"].stringValue
                        print(token)
                        var dialog = AZDialogViewController()
                        switch rCode {
                        case 200:
                            Config.token = token
                            self.checkUserWithToken()
                        case 400:
                            self.dismissHUD(isAnimated: true)
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Error de API")
                        case 401:
                            self.dismissHUD(isAnimated: true)
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Error de bases de datos")
                        case 404:
                            self.dismissHUD(isAnimated: true)
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Algo falla")
                        case 403:
                            self.dismissHUD(isAnimated: true)
                            dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                            dialog.show(in: self)
                            print("Credenciales no autorizadas")
                        default: break
                            
                        }
                        
                    } catch {
                        print(error)
                    }
                    
                    break
                case .failure(let error):
                    self.dismissHUD(isAnimated: true)
                    let dialog = AZDialogViewController(title: "", message: "Problemas de conexión")
                    dialog.show(in: self)
                    print(error)
                }
            }
        }
    }
    
    private func checkUserWithToken() {
        
        let params: [String:String] = [
            "UserName": UserDefaults.standard.getUserName(),
            "UserSecret": tvUserPassword.text!
        ]
        
        let headers: [String: String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "UserName":"ALKE0002",
            "Token": Config.token
        ]
        
        let urlString  = "http://infoquest.dyndns.org:8091/KEKUGAPI/LogIn"
        
        Alamofire.request(urlString, method: .post, parameters: params,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success:
                do {
                    let jsonData = try JSON(data: response.data!)
                    
                    let rCode = jsonData["responseCode"].intValue
                    let rMessage = jsonData["responseMessage"].stringValue
                    
                    let rData = jsonData["data"].arrayValue
                    print(jsonData)
                    for elements in rData {
                        
                        let UID = elements["UID"].intValue
                        let UName = elements["UName"].stringValue
                        let UEmail = elements["UEmail"].stringValue
                        
                        print("\(UID) : \(UName) : \(UEmail)")
                        
                        let rElements = elements["UModules"].arrayValue
                        
                        for ele in rElements {
                            let id = ele["ModuleId"].intValue
                            let mi = ele["ModuleImage"].stringValue
                            let mn = ele["ModuleName"].stringValue
                            
                            self.array_Modules.append(ModulesModel(moduleId: id, moduleName: mn, moduleImage: mi))
                        }
                    }
                    
                    var dialog = AZDialogViewController()
                    
                    
                    switch rCode {
                    case 200:
                        self.dismissHUD(isAnimated: true)
                        self.performSegue(withIdentifier: "segueHomeFromUser", sender: self)
                        //self.dismiss(animated: true, completion: nil)
                        print("Todo correcto")
                    case 400:
                        self.dismissHUD(isAnimated: true)
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Error de API")
                    case 401:
                        self.dismissHUD(isAnimated: true)
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Error de bases de datos")
                    case 404:
                        self.dismissHUD(isAnimated: true)
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Algo falla")
                    case 403:
                        self.dismissHUD(isAnimated: true)
                        dialog = AZDialogViewController(title: "\(rCode)", message: "\(rMessage)")
                        dialog.show(in: self)
                        print("Credenciales no autorizadas")
                    default: break
                        
                    }
                    
                } catch {
                    self.dismissHUD(isAnimated: true)
                    print(error)
                }
                
                break
            case .failure(let error):
                print("Error wea: \(error)")
                
            }
        }
        
        
    }

    @objc func goLoginVC() {
        let userVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        userVC.hero.isEnabled = true
        
        userVC.hero.modalAnimationType = .zoomSlide(direction: HeroDefaultAnimationType.Direction.left)
        self.hero.replaceViewController(with: userVC)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueHomeFromUser" {
            if let nVC = segue.destination as? UINavigationController {
                if let hVC = nVC.viewControllers.first as? HomeVC {
                    hVC.array_Modules = array_Modules
                }
            }
        }
    }
    
}
