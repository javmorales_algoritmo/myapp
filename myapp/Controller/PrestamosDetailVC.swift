//
//  PrestamosDetailVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/5/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import DJSemiModalViewController

class PrestamosDetailVC: UIViewController {

    @IBOutlet weak var collectionViewPago: TabularCollectionView!
    @IBOutlet weak var descuentosRestantes: UILabel!
    
    var data :[[String]] = [
        ["Concepto","Importe","Saldo"]
    ]
    
    var prestamosInformation = String()
    var saldoInformation = String()
    var descuentosRestantesInformation = String()
    var montoPrestamosInformation = String()
    var id_prestamo = Int()
    
    @IBOutlet weak var lbl_prestamo: UILabel!
    @IBOutlet weak var lbl_monto_prestamos: UILabel!
    @IBOutlet weak var lbl_saldo: UILabel!
    @IBOutlet weak var lbl_descuentos: UILabel!

    var prestamosDetailModelData = [PrestamosDetailModel]()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeCollection()
        title = "Detalles del Préstamo"
        descuentosRestantes.text = "Descuentos restantes"
        descuentosRestantes.adjustsFontSizeToFitWidth = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initializeCardTopView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.collectionViewPago.reloadData()
            NSLog("aca estamo")
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //data.removeAll()
        
        
    }

    
    private func initializeCardTopView() {
        lbl_prestamo.text = "\(prestamosInformation)"
        lbl_prestamo.adjustsFontSizeToFitWidth = true
        lbl_monto_prestamos.text = "\(montoPrestamosInformation)"
        lbl_saldo.text = "\(saldoInformation)"
        lbl_descuentos.text = "\(descuentosRestantesInformation)"
    }
    
    private func initializeCollection() {
        collectionViewPago.tabularDelegate = self
        collectionViewPago.tabularDatasource = self
        downloadAllMovimientos()
        //collectionViewPago.reloadData()
    }
    
    private func downloadAllMovimientos() {
        
        let headers: [String:String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "username":"ALKE0002",
            "token": Config.token
        ]
        
        let urlString  = "http://infoquest.dyndns.org:8091/KEKUGAPI/MovimientosPrestamo/\(id_prestamo)"
        
        Alamofire.request(urlString, method: .post,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success:
                do {
                    
                    let jsonData = try JSON(data: response.data!)
                    let rCode = jsonData["responseCode"].intValue
                    let rMessage = jsonData["responseMessage"].stringValue
                    
                    let rData = jsonData["data"].arrayValue
                    print("rCode: \(rCode) | rMessage: \(rMessage)")
                    
                    for elements in rData {
                        let id_movimiento = elements["id_movimiento"].intValue
                        let concepto = elements["concepto"].stringValue
                        let importe = elements["importe"].stringValue
                        let saldo_restante = elements["saldo_restante"].stringValue
                        let interes_ord = elements["interes_ord"].stringValue
                        let iva_interes_ord = elements["iva_interes_ord"].stringValue
                        let interes_mor = elements["interes_mor"].stringValue
                        let iva_interes_mor = elements["iva_interes_mor"].stringValue
                        let capital = elements["capital"].stringValue
                        
                        self.prestamosDetailModelData.append(PrestamosDetailModel(id_movimiento: id_movimiento, concepto: concepto, importe: importe, saldo_restante: saldo_restante, interes_ord: interes_ord, iva_interes_ord: iva_interes_ord, interes_mor: interes_mor, iva_interes_mor: iva_interes_mor, capital: capital))
                        
                        self.data.append([concepto,importe,saldo_restante])
                        
                    }

                } catch {
                    print(error)
                }
                    self.collectionViewPago.reloadData()
                break
            case .failure(let error):
                print("Error wea: \(error)")
                
            }
        }
    }
}

extension PrestamosDetailVC: TabularCollectionDataSource {
    
    func tabularView(_ tabularView: TabularCollectionView, titleAttributesForCellAt indexpath: IndexPath) -> CellTitleAttributes {
        var font = Font.avenirMedium.font()
        var textAlignment = NSTextAlignment.center
        if indexpath.section == 0 {
            font = Font.avenirHeavy.font(ofSize: 18)
        }
        if indexpath.row < 2 && indexpath.section != 0 {
            textAlignment = .center
        }
        //print("Section: \(data[indexpath.section]) Row: \([indexpath.row])")
        let text = data[indexpath.section][indexpath.row]//Checar error
        return CellTitleAttributes(title: text, font: font, textAlignment: textAlignment, textColor: Color.text.uiColor)
    }
    
    func numberOfColumns(in tabularView: TabularCollectionView) -> Int { return data.first?.count ?? 0 }
    
    func numberOfRows(in tabularView: TabularCollectionView) -> Int { return data.count }
    
    func numberOfStaticRows(in tabularView: TabularCollectionView) -> Int { return 1 }
    
    func numberOfStaticColumn(in tabularView: TabularCollectionView) -> Int { return 0 }
    
}

// MARK: - TabularCollectionDelegate

extension PrestamosDetailVC: TabularCollectionDelegate {
    
    @objc(tabularView:didSelectItemAt:) func tabularView(_ tabularView: TabularCollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            let detallesImportes = storyboard?.instantiateViewController(withIdentifier: "DetallesImportes") as! DetallesImportesVC
            
            detallesImportes.capitalText = prestamosDetailModelData[indexPath.section - 1].capital
            detallesImportes.interesText = prestamosDetailModelData[indexPath.section - 1].interes_ord
            detallesImportes.iva_interes_txt = prestamosDetailModelData[indexPath.section - 1].iva_interes_ord
            detallesImportes.interes_mor_txt = prestamosDetailModelData[indexPath.section - 1].interes_mor
            detallesImportes.iva_interes_mor_txt = prestamosDetailModelData[indexPath.section - 1].iva_interes_mor
            detallesImportes.total_txt = prestamosDetailModelData[indexPath.section - 1].importe
            
            navigationController?.pushViewController(detallesImportes, animated: true)
        }
    }
    
    func tabularView(_ tabularView: TabularCollectionView, shouldHideColumnSeparatorAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 0
    }
}
