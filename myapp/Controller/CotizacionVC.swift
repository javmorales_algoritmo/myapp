//
//  CotizacionVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/19/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import TKSubmitTransition
import Alamofire
import SwiftyJSON
import AZDialogView
import PopupDialog

class CotizacionVC: UIViewController {

    var id_producto = String()
    var tasa_ordinaria_txt = String()
    var nombre_prestamo_txt = String()
    
    let nombre_prestamo: UILabel = {
        let lbl = UILabel()
        lbl.text = "Nombre prestamo"
        lbl.setTextFontMedium(sizeFont: 17.0)
        lbl.contentMode = .left
        return lbl
    }()
    
    let tasa_ordinaria_title: UILabel = {
        let lbl = UILabel()
        lbl.text = "Tasa ordinaria anual"
        lbl.setTextFontMedium(sizeFont: 17.0)
        lbl.contentMode = .left
        return lbl
    }()
    
    let tasa_ordinaria: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 15.0)
        lbl.contentMode = .left
        return lbl
    }()
    
    let monto_solicitado: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField()
        txt.placeholder = "Monto a solicitar"
        txt.title = "Monto a solicitar"
        txt.selectedTitleColor = UIColor.black
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.keyboardType = .numberPad
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    
    let plaza_solicitado: SkyFloatingLabelTextField = {
        let txt = SkyFloatingLabelTextField()
        txt.placeholder = "Plazo a solicitar(Quincenas)"
        txt.title = "Plazo a solicitar(Quincenas)"
        txt.lineHeight = 1.0 // bottom line height in points
        txt.selectedLineHeight = 2.0
        txt.keyboardType = .numberPad
        txt.addDoneButtonOnKeyboard()
        return txt
    }()
    //UIScreen.main.bounds.size.width - 104
    let btn_cotizar: TKTransitionSubmitButton = {
        let btn = TKTransitionSubmitButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 64, height: 54))
        btn.addTarget(self, action: #selector(sendDatas(_:)), for: .touchUpInside)
        btn.layer.cornerRadius = btn.frame.height / 2
        //btn.backgroundColor = UIColor(r: 188.0, g: 178.0, b: 203.0)
        btn.backgroundColor = UIColor.colorWithHexString(hexStr: "#242E30")
        btn.setTitle("Cotizar", for: UIControl.State())
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 20)
        return btn
    }()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    var array_prod_cotizador = [CotizadorModel]()
    var data: [[String]] = [
        ["Monto\nmáximo","Plazo","Pago\nfijo"]
    ]
    
    @IBOutlet weak var cardPresentation: CardView!
    @IBOutlet weak var cvTableDatas: TabularCollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Cotización"
        hideKeyboardWhenTappedAround()
        setup()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.cvTableDatas.reloadData()
        })
    }
    
    private func setup() {
        
        [cardPresentation].forEach {view.addSubview($0) }
        
        [nombre_prestamo, tasa_ordinaria_title, tasa_ordinaria, monto_solicitado, plaza_solicitado, btn_cotizar].forEach {cardPresentation.addSubview($0) }
    
        nombre_prestamo.anchor(top: cardPresentation.topAnchor, leading: cardPresentation.leadingAnchor, bottom: nil, trailing: cardPresentation.trailingAnchor,padding: .init(top: 6, left: 8, bottom: 0, right: 8),size: .init(width: 0, height: 25))
        
        tasa_ordinaria_title.anchor(top: nombre_prestamo.bottomAnchor, leading: nombre_prestamo.leadingAnchor, bottom: nil, trailing: nombre_prestamo.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 35))
        
        tasa_ordinaria.anchor(top: tasa_ordinaria_title.bottomAnchor, leading: cardPresentation.leadingAnchor, bottom: nil, trailing: cardPresentation.trailingAnchor, padding: .init(top: 0, left: 32, bottom: 0, right: 16), size: .init(width: 0, height: 25))
        
        monto_solicitado.anchor(top: tasa_ordinaria.bottomAnchor, leading: cardPresentation.leadingAnchor, bottom: nil, trailing: tasa_ordinaria.trailingAnchor, padding: .init(top: 3, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 40))
        
        plaza_solicitado.anchor(top: monto_solicitado.bottomAnchor, leading: monto_solicitado.leadingAnchor, bottom: nil, trailing: monto_solicitado.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 16), size: .init(width: 0, height: 40))
        
        btn_cotizar.anchor(top: plaza_solicitado.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 55))
        
        
        nombre_prestamo.text = "\(nombre_prestamo_txt)"
        tasa_ordinaria.text = "\(tasa_ordinaria_txt) %"
        cvTableDatas.tabularDelegate = self
        cvTableDatas.tabularDatasource = self
        cvTableDatas.isHidden = true
        
    }
    
    @objc func sendDatas(_ sender: Any) {
        
        self.cvTableDatas.isHidden = true  
        checkCotizacion()
        //didStartYourLoading()
    }
    
    private func stop() {
        btn_cotizar.setOriginalState()
    }
    
    private func checkCotizacion() {
        
        if (monto_solicitado.text?.isEmpty)! || (plaza_solicitado.text?.isEmpty)! {
            
            showStandardDialog(animated: true)
        }else{
            showHUD(progressLabel: "Cargando")
            let headers: [String:String] = [
                "Content-Type":"application/x-www-form-urlencoded",
                "username":"ALKE0002",
                "token": Config.token
            ]
            
            let _ : [String:String] = [
                "idtrab":"\(UserDefaults.standard.getUserID())",
                "idprod":"\(id_producto)",
                "monto": monto_solicitado.text!,
                "plazo": plaza_solicitado.text!
            ]
            
            let result = Int(plaza_solicitado.text!)!*15
            let urlString  = "http://infoquest.dyndns.org:8091/ALGUGAPI/Cotizador/?idtrab=\(UserDefaults.standard.getUserID())&idprod=\(id_producto)&monto=\(monto_solicitado.text!)&plazo=\(result)"
            print(urlString)
            Alamofire.request(urlString, method: .post,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
                
                switch response.result {
                case .success:
                    do {
                        self.data.removeAll()
                        self.data.append(["Monto\nmáximo","Plazo","Pago\nfijo"])
                        let jsonData = try JSON(data: response.data!)
                        let rCode = jsonData["responseCode"].intValue
                        let rMessage = jsonData["responseMessage"].stringValue
                        let rData = jsonData["data"].arrayValue
                        
                        print("rCodeProductos: \(rCode) | rMessage: \(rMessage) rDatas: \(rData)")
                        
                        for items in rData {
                            let plazo = items["plazo"].stringValue
                            let monto = items["monto"].stringValue
                            let pago = items["pago"].stringValue
                            let tasa_ord = items["tasa_ord"].stringValue
                        
                            self.array_prod_cotizador.append(CotizadorModel(pago: pago, plazo: plazo, monto: monto, tasa_ord: tasa_ord))
                            let aux_monto = Double(monto)
                            let aux_currency = self.setCurrency(pago: Int(aux_monto!))
                            
                            
                            let pago_currency = self.setCurrencyDouble(pago: Double(pago)!)
                            
                            self.data.append(["\(aux_currency)","\(plazo)","\(pago_currency)"])
                            
                        }
                        
                        self.cvTableDatas.reloadData()
                        self.cvTableDatas.isHidden = false
                        self.dismissHUD(isAnimated: true)
                        //self.didFinishYourLoading()
                        //let cotizacionDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CotizacionDetallesVC") as! CotizacionDetallesVC
                        
                        //cotizacionDetailVC.data = self.data
                        //self.navigationController?.pushViewController(cotizacionDetailVC, animated: true)
                        
                    } catch {
                        print(error)
                    }
                    
                    break
                case .failure(let error):
                    print("Error wea: \(error)")
                    
                }
            }
        }
    }
    
    func showStandardDialog(animated: Bool = true) {
        
        let title = "ERROR"
        let message = "¡Faltan algunos datos!"
        
        let popup = PopupDialog(title: title,
                                message: message,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: true,
                                hideStatusBar: true) {
                                    print("Completed")
        }
        
        let buttonOne = CancelButton(title: "OK") { }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: animated, completion: nil)
    }
    
    private func setCurrency(pago: Int) -> String {
        let pago_formatter = pago
        var aux:String = ""
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        
        if let formattedTipAmount = formatter.string(from: pago_formatter as NSNumber) {
            aux = formattedTipAmount
        }
        
        return aux
    }
    
    private func setCurrencyDouble(pago: Double) -> String {
        let pago_formatter = pago
        var aux:String = ""
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        
        if let formattedTipAmount = formatter.string(from: pago_formatter as NSNumber) {
            aux = formattedTipAmount
        }
        
        return aux
    }
    
    private func didStartYourLoading() {
        btn_cotizar.startLoadingAnimation()
    }
    
    private  func didFinishYourLoading() {
        btn_cotizar.startFinishAnimation(1) {
            let cDetalles = self.storyboard?.instantiateViewController(withIdentifier: "CotizacionDetalles") as! CotizacionDetallesVC
            self.navigationController?.pushViewController(cDetalles, animated: true)
        }
    }
}

extension CotizacionVC: TabularCollectionDataSource {
    
    func tabularView(_ tabularView: TabularCollectionView, titleAttributesForCellAt indexpath: IndexPath) -> CellTitleAttributes {
        var font = Font.avenirMedium.font()
        var textAlignment = NSTextAlignment.center
        if indexpath.section == 0 {
            font = Font.avenirHeavy.font(ofSize: 18)
        }
        if indexpath.row < 2 && indexpath.section != 0 {
            textAlignment = .center
        }
        let text = data[indexpath.section][indexpath.row]
        return CellTitleAttributes(title: text, font: font, textAlignment: textAlignment, textColor: Color.text.uiColor)
    }
    
    func numberOfColumns(in tabularView: TabularCollectionView) -> Int { return data.first?.count ?? 0 }
    
    func numberOfRows(in tabularView: TabularCollectionView) -> Int { return data.count }
    
    func numberOfStaticRows(in tabularView: TabularCollectionView) -> Int { return 0 }
    
    func numberOfStaticColumn(in tabularView: TabularCollectionView) -> Int { return 0 }
    
}

extension CotizacionVC: TabularCollectionDelegate {
    
    @objc(tabularView:didSelectItemAt:) func tabularView(_ tabularView: TabularCollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            print("Jalando wea")
        }
    }
    
    func tabularView(_ tabularView: TabularCollectionView, shouldHideColumnSeparatorAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 0
    }
}
