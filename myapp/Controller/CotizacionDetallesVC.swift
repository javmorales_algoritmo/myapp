//
//  CotizacionDetallesVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/20/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class CotizacionDetallesVC: UIViewController {
    
//    let cvCotizacionPagos: TabularCollectionView = {
//        let cv = TabularCollectionView()
//        return cv
//    }()
    @IBOutlet weak var cvCotizacionPagos: TabularCollectionView!
    
    var data :[[String]] = [
        ["Monto máximo","Plazo","Pago fijo"]
    ]
    
    var data2: [[String]] = []
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeCollection()
        
        dump(data)
    }
    
    private func initializeCollection() {
        
        view.addSubview(cvCotizacionPagos)
        
        cvCotizacionPagos.fillSuperview()
        
        cvCotizacionPagos.tabularDelegate = self
        cvCotizacionPagos.tabularDatasource = self
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            self.cvCotizacionPagos.reloadData()
        })
    }
}

extension CotizacionDetallesVC: TabularCollectionDataSource {
    
    func tabularView(_ tabularView: TabularCollectionView, titleAttributesForCellAt indexpath: IndexPath) -> CellTitleAttributes {
        var font = Font.avenirMedium.font()
        var textAlignment = NSTextAlignment.center
        if indexpath.section == 0 {
            font = Font.avenirHeavy.font(ofSize: 18)
        }
        if indexpath.row < 2 && indexpath.section != 0 {
            textAlignment = .center
        }
        let text = data[indexpath.section][indexpath.row]
        return CellTitleAttributes(title: text, font: font, textAlignment: textAlignment, textColor: Color.text.uiColor)
    }
    
    func numberOfColumns(in tabularView: TabularCollectionView) -> Int { return data.first?.count ?? 0 }
    
    func numberOfRows(in tabularView: TabularCollectionView) -> Int { return data.count }
    
    func numberOfStaticRows(in tabularView: TabularCollectionView) -> Int { return 0 }
    
    func numberOfStaticColumn(in tabularView: TabularCollectionView) -> Int { return 0 }
    
}

extension CotizacionDetallesVC: TabularCollectionDelegate {
    
    @objc(tabularView:didSelectItemAt:) func tabularView(_ tabularView: TabularCollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.section != 0 {
            //let detallesImportes = storyboard?.instantiateViewController(withIdentifier: "DetallesImportes") as! DetallesImportesVC
            
            //navigationController?.pushViewController(detallesImportes, animated: true)
        }
    }
    
    func tabularView(_ tabularView: TabularCollectionView, shouldHideColumnSeparatorAt indexPath: IndexPath) -> Bool {
        return indexPath.row != 0
    }
}
