//
//  DetallesImportesVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/13/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

class DetallesImportesVC: UIViewController {
    
    //var capitalText: String!
    var capitalText = String()
    var interesText = String()
    var iva_interes_txt = String()
    var interes_mor_txt = String()
    var iva_interes_mor_txt = String()
    var total_txt = String()
    
    let titleTxt: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "AvenirNext-Medium", size: 25.0)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let capitallbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let intereslbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let iva_interes_lbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let interes_mor_lbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let iva_interes_mor_lbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let total_lbl: UILabel = {
        let lbl = UILabel()
        lbl.setTextFontRegular(sizeFont: 20.0)
        lbl.textAlignment = .left
        return lbl
    }()

    
    let card_view: CardView = {
        let v = CardView()
        v.backgroundColor = UIColor.white
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Detalles"
        setImportsDetail()
    }
    
    private func setImportsDetail() {
        titleTxt.text = "Importes"
        capitallbl.text = "Capital: \(capitalText)"
        intereslbl.text = "Interés: \(interesText)"
        iva_interes_lbl.text = "IVA de interés: \(iva_interes_txt)"
        interes_mor_lbl.text = "Interés moratorio: \(interes_mor_txt)"
        iva_interes_mor_lbl.text = "IVA de interés moratorio: \(iva_interes_mor_txt)"
        total_lbl.text = "Importe total: \(total_txt)"
        
        [card_view].forEach { view.addSubview($0) }
        
        if #available(iOS 11.0, *) {
            card_view.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24), size: .init(width: 0, height: 400))
        } else {
            card_view.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 24, bottom: 0, right: 24), size: .init(width: 0, height: 400))
        }
        
        
        if capitalText.isEmpty {
            
            [total_lbl].forEach {card_view.addSubview($0) }
            total_lbl.text = "Importe total: \(total_txt)"
            total_lbl.anchor(top: card_view.topAnchor, leading: card_view.leadingAnchor, bottom: nil, trailing: card_view.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 60 ))
        }else{
            
            if interes_mor_txt.isEmpty && iva_interes_mor_txt.isEmpty {
                [capitallbl, intereslbl, iva_interes_lbl, total_lbl].forEach {card_view.addSubview($0) }
                
                capitallbl.anchor(top: card_view.topAnchor, leading: card_view.leadingAnchor, bottom: nil, trailing: card_view.trailingAnchor, padding: .init(top: 8, left: 12, bottom: 0, right: 12), size: .init(width: 0, height: 50))
                
                intereslbl.anchor(top: capitallbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                iva_interes_lbl.anchor(top: intereslbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                total_lbl.anchor(top: iva_interes_lbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
            }else{
                
                [capitallbl, intereslbl, iva_interes_lbl, interes_mor_lbl, iva_interes_mor_lbl, total_lbl].forEach {card_view.addSubview($0) }
                
                capitallbl.anchor(top: card_view.topAnchor, leading: card_view.leadingAnchor, bottom: nil, trailing: card_view.trailingAnchor, padding: .init(top: 8, left: 12, bottom: 0, right: 12), size: .init(width: 0, height: 50))
                
                intereslbl.anchor(top: capitallbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                iva_interes_lbl.anchor(top: intereslbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                interes_mor_lbl.anchor(top: iva_interes_lbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                iva_interes_mor_lbl.anchor(top: interes_mor_lbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
                total_lbl.anchor(top: iva_interes_mor_lbl.bottomAnchor, leading: capitallbl.leadingAnchor, bottom: nil, trailing: capitallbl.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 50))
                
            }
        }
    }
}
