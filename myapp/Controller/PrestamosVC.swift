//
//  PrestamosVC.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/27/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class PrestamosVC: UIViewController {
    
    var cvCardsPrestamos: UICollectionView!
    
    let identifierCard = "cellPrestamos"
    
    var datasPrestamos = [PrestamosModel]()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        
        setupCollectionView()
        downloadAllPrestamos()
        title = "Préstamos"
        
    }
    
    private func setupCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: 344, height: 250)
        
        cvCardsPrestamos = UICollectionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), collectionViewLayout: layout)
        
        cvCardsPrestamos.isUserInteractionEnabled = true
        cvCardsPrestamos.dataSource = self
        cvCardsPrestamos.delegate = self
        cvCardsPrestamos.register(UINib(nibName: "cellPrestamos", bundle: nil), forCellWithReuseIdentifier: identifierCard)
        //collectionViewModules!.register(ModulesCell.self, forCellWithReuseIdentifier: identifier)
        cvCardsPrestamos!.backgroundColor = UIColor.colorWithHexString(hexStr: "#EBECED")
        self.view.addSubview(cvCardsPrestamos!)
        
        cvCardsPrestamos.fillSuperview()
        
    }
    
    private func downloadAllPrestamos() {

        let headers: [String:String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "username":"ALKE0002",
            "token": Config.token
        ]

        let urlString  = "http://infoquest.dyndns.org:8091/KEKUGAPI/PrestamosTrabajador/\(UserDefaults.standard.getUserName())"
        
        Alamofire.request(urlString, method: .post,encoding: URLEncoding.httpBody, headers: headers ).responseJSON { response in
            
            switch response.result {
            case .success:
                do {
                    let jsonData = try JSON(data: response.data!)
                    let rCode = jsonData["responseCode"].intValue
                    let rMessage = jsonData["responseMessage"].stringValue
                    
                    let rData = jsonData["data"].arrayValue
                    print("rCode: \(rCode) | rMessage: \(rMessage)")
                    
                    for elements in rData {
                        let id_prestamo = elements["id_prestamo"].intValue
                        let nombre_prestamo = elements["nombre_prestamo"].stringValue
                        let monto_prestamo = elements["monto_prestamo"].stringValue
                        let total_pagar = elements["total_pagar"].stringValue
                        let saldo_restante = elements["saldo_restante"].stringValue
                        let descuentos_restantes = elements["descuentos_restantes"].stringValue
                        let estatus_credito = elements["estatus_credito"].stringValue
                        self.datasPrestamos.append(PrestamosModel(id_prestamo: id_prestamo, nombre_prestamo: nombre_prestamo, monto_prestamo: monto_prestamo, total_pagar: total_pagar, saldo_restante: saldo_restante, descuentos_restantes: descuentos_restantes, estatus_credito: estatus_credito))
                        print("Estatua creditos: \(estatus_credito)")
                    }
                    self.cvCardsPrestamos.reloadData()
                } catch {
                    print(error)
                }
                
                break
            case .failure(let error):
                print("Error wea: \(error)")
                
            }
        }
    }
}

extension PrestamosVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //segueToDetailHome
        let prestamosDVC = storyboard?.instantiateViewController(withIdentifier: "PrestamosDetailVC") as! PrestamosDetailVC
        
        prestamosDVC.prestamosInformation = datasPrestamos[indexPath.row].nombre_prestamo
        prestamosDVC.saldoInformation = datasPrestamos[indexPath.row].saldo_restante
        prestamosDVC.montoPrestamosInformation = datasPrestamos[indexPath.row].monto_prestamo
        prestamosDVC.descuentosRestantesInformation = datasPrestamos[indexPath.row].descuentos_restantes
        prestamosDVC.id_prestamo = datasPrestamos[indexPath.row].id_prestamo
        
        navigationController?.pushViewController(prestamosDVC, animated: true)
    }
}

extension PrestamosVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasPrestamos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierCard, for: indexPath) as! PrestamosMenuCell
        
        cell.prestamosInfo.text = datasPrestamos[indexPath.row].nombre_prestamo
        cell.prestamosInfo.adjustsFontSizeToFitWidth = true
        cell.saldoInfo.text = datasPrestamos[indexPath.row].saldo_restante
        cell.descuentosInfo.text = datasPrestamos[indexPath.row].descuentos_restantes
        cell.descuentosInfo.adjustsFontSizeToFitWidth = true
        
        cell.descuentosRestanteslbl.numberOfLines = 0
        cell.descuentosRestanteslbl.text = "Descuentos\nRestantes"
        cell.estatusInfo.isHidden = true
        cell.estatusInfoEstatico.isHidden = true
        
        //cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.width)
        
       
        
        if datasPrestamos[indexPath.row].estatus_credito != "ACTIVO" {
            cell.estatusInfo.isHidden = false
            cell.estatusInfoEstatico.isHidden = false
            //cell.descuentosRestanteslbl.adjustsFontSizeToFitWidth = true
            cell.descuentosRestanteslbl.numberOfLines = 0
        }
        cell.estatusInfo.text = "\(datasPrestamos[indexPath.row].estatus_credito)"
        
        //cell.contentView.layer.cornerRadius = 4.0
        //cell.contentView.layer.borderWidth = 1.0
        //cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        //cell.contentView.layer.masksToBounds = false
        //cell.layer.masksToBounds = false
        //cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        return cell
    }
    
    
}
