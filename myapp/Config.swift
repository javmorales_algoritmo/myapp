//
//  Config.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/7/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation

class Config {
    
    static var token:String = ""
    static var user_name:String = ""
    static let key = ""
    
    static public func getOSInfo() -> String {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
}
