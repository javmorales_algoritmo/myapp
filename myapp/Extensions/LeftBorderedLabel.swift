//
//  SeparatorStackView.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/4/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import UIKit

@IBDesignable
class LeftBorderedLabel: UILabel {
    
    @IBInspectable var blockColor: UIColor = UIColor.black {
        
        didSet{
            
            let border = CALayer()
            //border.frame = CGRect(x: 0, y: 0, width: 15, height: self.frame.height)
            border.frame = CGRect(x: self.bounds.width - 1.0, y: 0,  width: 1.0, height: self.bounds.height)
            
            border.backgroundColor = blockColor.cgColor;
            
            self.layer.addSublayer(border)
        }
    }
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
    }
}
