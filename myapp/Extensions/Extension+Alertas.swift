//
//  ExtensionMBProgressHUD.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/22/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation
import MBProgressHUD
import PopupDialog

extension UIViewController {
    func showHUD(progressLabel:String){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = progressLabel
        progressHUD.mode = MBProgressHUDMode.indeterminate
    }
    
    func dismissHUD(isAnimated:Bool) {
        MBProgressHUD.hide(for: self.view, animated: isAnimated)
    }

    func showStandardDialog(animated: Bool = true, title: String, mess: String) {
        
        let title = "\(title)"
        let message = "\(mess)"
        
        let popup = PopupDialog(title: title,
                                message: message,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: true,
                                hideStatusBar: true) {
                                    print("Completed")
        }
        
        let buttonOne = CancelButton(title: "OK") { }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: animated, completion: nil)
    }
}
