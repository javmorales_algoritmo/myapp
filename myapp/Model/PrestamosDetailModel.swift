//
//  PrestamosDetailModel.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/7/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation

struct PrestamosDetailModel {
    
    let id_movimiento:Int
    let concepto:String
    let importe:String
    let saldo_restante:String
    let interes_ord:String
    let iva_interes_ord:String
    let interes_mor:String
    let iva_interes_mor:String
    let capital:String
    
    init(id_movimiento:Int, concepto:String, importe:String, saldo_restante:String, interes_ord:String,iva_interes_ord:String, interes_mor:String, iva_interes_mor:String, capital:String) {
        self.id_movimiento = id_movimiento
        self.concepto = concepto
        self.importe = importe
        self.saldo_restante = saldo_restante
        self.interes_ord = interes_ord
        self.iva_interes_ord = iva_interes_ord
        self.interes_mor = interes_mor
        self.iva_interes_mor = iva_interes_mor
        self.capital = capital
    }
}
