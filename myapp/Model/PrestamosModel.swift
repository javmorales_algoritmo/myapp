//
//  PrestamosModel.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/5/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation

struct PrestamosModel {
    let id_prestamo: Int
    let nombre_prestamo: String
    let monto_prestamo: String
    let total_pagar: String
    let saldo_restante: String
    let descuentos_restantes: String
    let estatus_credito: String
    
    init(id_prestamo: Int, nombre_prestamo: String, monto_prestamo: String, total_pagar: String, saldo_restante:String, descuentos_restantes:String, estatus_credito: String) {
        self.id_prestamo = id_prestamo
        self.nombre_prestamo = nombre_prestamo
        self.monto_prestamo = monto_prestamo
        self.total_pagar = total_pagar
        self.saldo_restante = saldo_restante
        self.descuentos_restantes = descuentos_restantes
        self.estatus_credito = estatus_credito
    }
}
