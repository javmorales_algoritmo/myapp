//
//  HomeModel.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/8/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation

struct ModulesModel {
    
    let moduleId: Int
    let moduleName: String
    let moduleImage: String
    
    init(moduleId: Int, moduleName: String, moduleImage: String) {
        self.moduleId = moduleId
        self.moduleName = moduleName
        self.moduleImage = moduleImage
    }
}
