//
//  CotizadorModel.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/20/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation
struct CotizadorModel {
    let pago: String
    let plazo: String
    let monto: String
    let tasa_ord:String
    
    init(pago:String, plazo:String, monto:String, tasa_ord: String) {
        self.pago = pago
        self.plazo = plazo
        self.monto = monto
        self.tasa_ord = tasa_ord
    }
}
