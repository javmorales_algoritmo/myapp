//
//  Objecto.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 11/4/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation

struct Object {
   
    let contact: String
    let enabled: Bool
    let firstName: String
    let lastName: String
    
    
}
