//
//  ProductosModel.swift
//  myapp
//
//  Created by Victor Javier Arroyo Morales on 12/18/18.
//  Copyright © 2018 Victor Javier Arroyo Morales. All rights reserved.
//

import Foundation
//Detalles del prestamo

struct ProductosModel {
    let id_prod: String
    let nombre_producto: String
    let descripcion_producto: String
    let tasa_ord_max: String
    let tipo_tasa: String
    
    init(id_prod: String, nombre_producto: String, descripcion_producto: String, tasa_ord_max: String, tipo_tasa: String) {
        self.id_prod = id_prod
        self.nombre_producto = nombre_producto
        self.descripcion_producto = descripcion_producto
        self.tasa_ord_max = tasa_ord_max
        self.tipo_tasa = tipo_tasa
    }
}
